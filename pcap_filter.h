#ifndef PCAP_FILTER_H
#define PCAP_FILTER_H

#include <stdint.h>
#include <stddef.h>

#include <rte_config.h>
#include <rte_eal.h>
#include <rte_ethdev.h>
#include <rte_cycles.h>
#include <rte_lcore.h>
#include <rte_mbuf.h>
#include <rte_tcp.h>
#include <rte_ip.h>
#include <rte_rcu_qsbr.h>
#include <rte_compat.h>
#include <rte_hash.h>
#include "rte_jhash.h"

struct filter_settings {
  uint64_t flow_number;
};

enum IntermediateState {
  closed_is = 0x00u,
  sync_is = 0x01u,
  proven_is = 0x02u,
  fin_sent_is = 0x03u,
  fin_ack_is = 0x04u
};

enum FinalyState {
  DROP = 0x00u,
  RETRANSMISSION = 0x01u,
  ACCEPT = 0x02u
};

struct tcp_flow_key {
  uint32_t f_ip;   // first ip address
  uint32_t f_prt;  // first port
  uint32_t s_ip;   // second ip address
  uint32_t s_prt;  // second port
};

struct tcp_flow_state {
  enum IntermediateState state;
  uint16_t first_in_dialog;
};

struct fsm_state {
  enum IntermediateState phase;
  uint32_t active_flags;
};

// main methods
uint8_t accept_tcp(struct rte_mbuf* m);
uint8_t analyze_hdr(struct rte_tcp_hdr* tcp_header, struct tcp_flow_key* key, uint8_t direction);
void init_filter(struct filter_settings* settings);

// hash table methods
uint32_t hash_tcp_key(const void* k, uint32_t length, uint32_t initval);
struct tcp_flow_state* at_key_or_default(struct tcp_flow_key* key, struct tcp_flow_state* default_value);
uint8_t add_by_key(struct tcp_flow_key* key, struct tcp_flow_state* value);
uint8_t delete_by_key(struct tcp_flow_key* key);

#endif //PCAP_FILTER_H

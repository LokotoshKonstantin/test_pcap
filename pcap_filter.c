#include "pcap_filter.h"

#include "math.h"

#define FIN 0x01u
#define SYN 0x02u
#define ACK 0x10u
#define BCKWD 0x80u

static struct rte_hash* g_flows;
struct rte_mempool* g_tcp_flow_state_pool = NULL;
static struct rte_hash* fsm_transitions = NULL;
struct rte_mempool* fsm_states;

// Hash function of the address and port values of the TCP-flow participants
uint32_t hash_tcp_key(const void* k, uint32_t length, uint32_t initval) {
  struct tcp_flow_key* key = (struct tcp_flow_key*)k;
  uint32_t frs, scn;
  uint32_t const_value_size = sizeof(uint32_t);

  frs = (rte_jhash_32b(&key->f_ip, const_value_size, initval) + rte_jhash_32b(&key->f_prt, const_value_size, initval)) * 37;
  scn = (rte_jhash_32b(&key->s_ip, const_value_size, initval) + rte_jhash_32b(&key->s_prt, const_value_size, initval)) * 37;

  uint32_t result = frs + scn;
  return result;
}

// Getter Value by Key from Hash Table.
struct tcp_flow_state* at_key_or_default(struct tcp_flow_key* key, struct tcp_flow_state* default_value) {
  struct tcp_flow_state* flow_state;
  if (rte_hash_lookup_data(g_flows, key, (void**)&flow_state) < 0) {
    flow_state = default_value;
  }
  return flow_state;
}

// Add new value by key.
uint8_t add_by_key(struct tcp_flow_key* key, struct tcp_flow_state* value) {
  struct tcp_flow_state* tmp;
  if (rte_hash_lookup_data(g_flows, key, (void**)&tmp) < 0) {
    struct tcp_flow_state* new_element;
    rte_mempool_get(g_tcp_flow_state_pool, (void**)&new_element);

    new_element->state = value->state;
    new_element->first_in_dialog = value->first_in_dialog;

    rte_hash_add_key_data(g_flows, key, new_element);
    return 1;
  }
  return 0;
}

// Delete value by key and free memory.
uint8_t delete_by_key(struct tcp_flow_key* key) {
  struct tcp_flow_state* flow_state;
  if (rte_hash_lookup_data(g_flows, key, (void**)&flow_state) < 0)
    return 0;

  rte_hash_del_key(g_flows, key);
  rte_mempool_put(g_tcp_flow_state_pool, flow_state);
  return 1;
}

// Helper method
static inline void add_new_transition(enum IntermediateState phase_from, uint32_t flags_from, enum IntermediateState phase_to) {
  struct fsm_state t = {
      .phase = phase_from,
      .active_flags = flags_from
  };
  enum IntermediateState* s;
  rte_mempool_get(fsm_states, (void**)&s);
  *s = phase_to;
  rte_hash_add_key_data(fsm_transitions, &t, s);
}

// Adding new path in FSM from prev state to next state
static inline void add_new_path(enum IntermediateState phase_from, uint32_t flags_from, enum IntermediateState phase_to) {
  add_new_transition(phase_from, flags_from, phase_to);
  add_new_transition(phase_to, BCKWD, phase_from);
}

// INicialization FSM
static void init_fsm() {
  struct rte_hash_parameters hash_params = {
      .entries = 100,
      .key_len = sizeof(struct fsm_state),
      .socket_id = (int)rte_socket_id(),
      .hash_func_init_val = 0,
      .name = "fsm"
  };
  fsm_transitions = rte_hash_create(&hash_params);
  if (fsm_transitions == NULL) {
    rte_exit(EXIT_FAILURE, "No FSM created\n");
  }

  fsm_states = rte_mempool_create("FSM states", 20,
                                  sizeof(enum IntermediateState),0, 0, NULL, NULL,
                                  NULL, NULL, (int)rte_socket_id(), 0);

  add_new_path(closed_is, SYN, sync_is);
  add_new_path(sync_is, ACK, proven_is);
  add_new_path(proven_is, FIN + ACK, fin_sent_is);
  add_new_path(fin_sent_is, ACK, fin_ack_is);
}

// Checking for repetition
static uint8_t is_retransmission(struct fsm_state* state) {
  enum IntermediateState* tmp;
  if (rte_hash_lookup_data(fsm_transitions, state, (void**)&tmp) < 0) {
    struct fsm_state b_key = {
        .active_flags = BCKWD,
        .phase = state->phase
    };
    if (rte_hash_lookup_data(fsm_transitions, &b_key, (void**)&tmp) < 0)
      return 0;
    else {
      state->phase = *tmp;
      is_retransmission(state);
    }
  }
  return 1;
}

// Get next state of FSM
static enum FinalyState get_next_state(struct fsm_state* state) {
  enum IntermediateState* tmp;
  if (rte_hash_lookup_data(fsm_transitions, state, (void**)&tmp) < 0) {
    struct fsm_state b_key = {
        .active_flags = BCKWD,
        .phase = state->phase
    };

    if (rte_hash_lookup_data(fsm_transitions, &b_key, (void**)&tmp) < 0) {
      return DROP;
    }

    struct fsm_state prev_state = {
        .phase = *tmp,
        .active_flags = state->active_flags
    };
    return is_retransmission(&prev_state);
  }
  state->phase = *tmp;
  return ACCEPT;
}

// Анализ одного пакета
uint8_t analyze_hdr(struct rte_tcp_hdr* tcp_header, struct tcp_flow_key* key, uint8_t direction) {
  struct tcp_flow_state default_value = {
      .state = closed_is,
      .first_in_dialog = direction
  };
  struct tcp_flow_state* flow_state = at_key_or_default(key, &default_value);

  if (flow_state->state == proven_is && ((tcp_header->tcp_flags & FIN) == 0)) {
    return 1;
  }

  struct fsm_state t = {
      .phase = flow_state->state,
      .active_flags = (tcp_header->tcp_flags & SYN) + (tcp_header->tcp_flags & ACK) + (tcp_header->tcp_flags & FIN)
  };

  switch (get_next_state(&t)) {
    case DROP : {
      return 0;
    }

    case RETRANSMISSION : {
      return 1;
    }

    case ACCEPT : {
      flow_state->state = t.phase;
      if (flow_state->state == sync_is){
        add_by_key(key, flow_state);
      }
      return 1;
    }

    default:{
      rte_exit(EXIT_FAILURE, "ERROR:: Unconditional state");
    }
  }
}

// Парсинг заголовков и запуск анализатора для пакета
uint8_t accept_tcp(struct rte_mbuf* m) {
  struct rte_ether_hdr* eth_header = rte_pktmbuf_mtod(m, struct rte_ether_hdr*);

  struct rte_arp_ipv4* ip_header =
      (struct rte_arp_ipv4 *) ((char *) eth_header + sizeof(struct rte_ether_hdr));
  uint32_t src_addr = ip_header->arp_sip;
  uint32_t dst_addr = ip_header->arp_tip;

  struct rte_tcp_hdr* tcp_header = (struct rte_tcp_hdr *) ((char *) ip_header + sizeof(struct rte_ipv4_hdr));
  uint16_t src_port = tcp_header->src_port;
  uint16_t dst_port = tcp_header->dst_port;

  uint8_t direction = src_addr < dst_addr;

  struct tcp_flow_key key;
  if (direction) {
    key.f_ip = src_addr;
    key.f_prt = src_port;

    key.s_ip = dst_addr;
    key.s_prt = dst_port;

  } else {
    key.f_ip = dst_addr;
    key.f_prt = dst_port;

    key.s_ip = src_addr;
    key.s_prt = src_port;
  }
  return analyze_hdr(tcp_header, &key, direction);
}

// Инициализирует значения для фильтра (hash-table и конечный атомат для парсинга)
void init_filter(struct filter_settings* settings) {
  init_fsm();
  struct rte_hash_parameters hash_params = {
      .entries = settings->flow_number,
      .key_len = sizeof(struct tcp_flow_key),
      .socket_id = (int)rte_socket_id(),
      .hash_func_init_val = 0,
      .hash_func = hash_tcp_key,
      .name = "TCP Hash Table"
  };
  g_flows = rte_hash_create(&hash_params);
  if (g_flows == NULL)
    { rte_exit(EXIT_FAILURE, "No hash table created\n"); }

  g_tcp_flow_state_pool = rte_mempool_create("Mempool", settings->flow_number + 1000,
                                             sizeof(struct tcp_flow_state),0, 0, NULL, NULL,
                                             NULL, NULL, (int)rte_socket_id(), 0);
}
